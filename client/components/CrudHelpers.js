import { diff } from 'rus-diff'

export const ConfirmDelete = (that, id, method) => {
    that.$buefy.dialog.confirm({
        title: `Delete ${that.singular}`,
        message: `Are you sure you want to <b>delete</b> that ${that.singular}? This action cannot be undone.`,
        confirmText: `Delete ${that.singular}`,
        type: 'is-danger',
        hasIcon: true,
        onConfirm: () => {
            Meteor.call(method, id, (error) => {
                if (error) {
                    that.$buefy.notification.open(error)
                } else {
                    that.$buefy.toast.open(`${that.singular} deleted!`)
                }                 
            })
        }
    })
}

export const save = (event, that, method) => {
    event.preventDefault()
    var differences = diff(that.original, that.doc)
    // console.log('diff', differences)
    if (differences) {
        const _id = that.isNew ? that.doc.code : that.$route.params.id
        Meteor.call(method, _id, differences, (error) => {
            if (error) {
                that.$buefy.notification.open(error)
            } else {
                that.$buefy.notification.open('Saved')
                that.$router.push(`/${that.plural}`)
            }                 
        })
    } else {
        that.$router.push(`/${that.plural}`)
    }
}
