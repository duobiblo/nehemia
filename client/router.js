import Vue from 'vue'
// import { MeteorData } from 'vue-meteor-tracker'
import VueRouter from 'vue-router'

const Home = async () => import('./pages/Home.vue')
const ContactRequests = async () => import('./pages/ContactRequests.vue')
const ContactRequest = async () => import('./pages/ContactRequest.vue')
const BibleInfos = async () => import('./pages/BibleInfos.vue')
const BibleInfo = async () => import('./pages/BibleInfo.vue')
const BibleSocieties = async () => import('./pages/BibleSocieties.vue')
const BibleSociety = async () => import('./pages/BibleSociety.vue')
const Scrapers = async () => import('./pages/Scrapers.vue')
const Scraper = async () => import('./pages/Scraper.vue')
const Logs = async () => import('./pages/Logs.vue')
const NotFound = async () => import('./pages/NotFound.vue')

export const routes = [
  { path: '/', name: 'home', component: Home },
  { path: '/contacts', name: 'contacts', component: ContactRequests },
  { path: '/contacts/:id', name: 'contact', component: ContactRequest },
  { path: '/bibleInfos', name: 'bibleInfos', component: BibleInfos },
  { path: '/bibleInfos/:id', name: 'bibleInfo', component: BibleInfo },
  { path: '/bibleSocieties', name: 'bibleSocieties', component: BibleSocieties },
  { path: '/bibleSocieties/:id', name: 'bibleSociety', component: BibleSociety },
  { path: '/scrapers', name: 'scrapers', component: Scrapers },
  { path: '/scrapers/:id', name: 'scraper', component: Scraper },
  { path: '/logs', name: 'logs', component: Logs },
  { path: '*', name: 'not-found', component: NotFound },
]

Vue.use(VueRouter)

export default router = new VueRouter({
    mode: 'history',
    routes,
})