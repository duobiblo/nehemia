import Vue from 'vue'
import VueMeteorTracker from 'vue-meteor-tracker'
Vue.use(VueMeteorTracker)

import Buefy from 'buefy'

Vue.use(Buefy, {
    // defaultFieldLabelPosition: 'on-border',
})

import App from './App.vue'
import router from './router.js'
import { store } from '../store/store.js'
import Helpers from '../lib/helpers.js'

Vue.mixin({
    methods: Object.assign({
        user: () => store.state.user,
    }, Helpers)
})
  
Meteor.startup(() => {
    new Vue({
        el: '#app',
        ...App,
        router,
        store,
    })
})
