nl2br = (text = '') => text.replace(/\n/g, '<br>')

// export const formatDateTime = date => date && date.toLocaleString('es-PY', { hour12: false })
formatDateTime = (date = new Date()) => date && date.toLocaleString('es-PY', { hour12: false })

promisify = (fn, context) =>
    (...args) =>
        new Promise((resolve, reject) => {
            callback = (error, data) => error ? reject(error) : resolve(data)
            return fn.call(context, ...args, callback)
        })

asyncMethod = promisify(Meteor.call, Meteor)

export default { nl2br, formatDateTime, promisify, asyncMethod }