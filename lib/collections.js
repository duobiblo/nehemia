import { Mongo } from 'meteor/mongo'

ContactForm = new Mongo.Collection('ContactForm')
Stats = new Mongo.Collection('Stats')
InstalledTranslationsCounter = new Mongo.Collection('InstalledTranslationsCounter')
BibleInfos = new Mongo.Collection('BibleInfos')
BibleSocieties = new Mongo.Collection('BibleSocieties')
Scrapers = new Mongo.Collection('Scrapers')
JobResults = new Mongo.Collection('JobResults')

