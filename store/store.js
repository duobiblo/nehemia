import { Meteor } from 'meteor/meteor'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        user: null,
    },
    mutations: {
        setUser(state, user) {
            state.user = user
        },
        clearUser(state) {
            state.user = null
        }
    }
})
  
// warm up store after restart
Meteor.startup(async () => {
    const user = await asyncMethod('getUserFromId')
    store.commit('setUser', user)
})
