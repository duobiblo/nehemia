import { Meteor } from 'meteor/meteor'
import { check, Match } from 'meteor/check'
import crypto, { sign } from 'crypto'
import { spawn } from 'child_process'

const checkLoggedIn = () => {
    if (!Meteor.userId()) throw new Meteor.Error(401, 'You are not logged in')
}

Meteor.methods({
    'scrape.runScript' (doc) {
        checkLoggedIn()
        check(doc, Object)
        const ticket = crypto.randomBytes(16).toString("hex")
        try {
            run(ticket, doc)
            return ticket
        } catch (error) {
            JobResults.update(ticket, { errorStatus: error.status, errorMessage: error.message })
            throw new Meteor.Error(error.status, error.message)
        }
    },
    'scrape.stopScript' () {
        checkLoggedIn()
        prc && prc.kill(8)
    },
})

let prc

const run = (ticket, doc) => {
    const [cwd, ...args] = doc.text2LookupScript
        .split('\n')
        .map(l => l.split(' '))
        .flat()

    let log = ''

    const start = process.hrtime()
    JobResults.insert({ _id: ticket, start: new Date() })
    prc = spawn('node', args, { cwd })
    
    prc.stdout.on('data', Meteor.bindEnvironment(data => {
        log += data.toString()
        JobResults.update(ticket, { text: log })
    }))

    prc.stderr.on('data', Meteor.bindEnvironment(data => {
        log += data.toString()
        JobResults.update(ticket, { text: log })
    }))
    
    prc.on('close', Meteor.bindEnvironment((code, signal) => {
        const file = args[args.findIndex(a => a == '--out') + 1]
        const time = hrTimeToSeconds(process.hrtime(start))
        const action = signal ? 'Aborted' : 'Finished'
        log += `${action} after ${time} seconds`
        JobResults.update(ticket, { text: log, end: new Date(), time })
        prc = null
    }))
}

const hrTimeToSeconds = hrtime => seconds = (hrtime[0] + (hrtime[1] / 1e9)).toFixed(3)
