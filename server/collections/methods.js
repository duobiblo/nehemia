import { Meteor } from 'meteor/meteor'
import { check, Match } from 'meteor/check'
import { sendEmail } from '../email.js'
import bibleInfos from './bibleInfos.js'

const checkLoggedIn = () => {
    if (!Meteor.userId()) throw new Meteor.Error(401, 'You are not logged in')
}

Meteor.methods({
    'contactForms.remove' (_id) {
        checkLoggedIn()
        check(_id, String)
        ContactForm.remove(_id)
    },
    'stats.remove' (_id) {
        checkLoggedIn()
        check(_id, String)
        Stats.remove(_id)
    },
    'bibleInfos.remove' (_id) {
        checkLoggedIn()
        check(_id, String)
        BibleInfos.remove(_id)
    },
    'bibleInfos.save' (_id, $set) {
        checkLoggedIn()
        check(_id, String)
        check($set, Object)
        BibleInfos.upsert(_id, $set)
    },
    'bibleInfos.changePrimaryLanguageChoice' (bible, value) {
        checkLoggedIn()
        check(bible, Object)
        check(value, Boolean)
        if (!bible.lang) return
        if (value) {
            BibleInfos.update({ lang: bible.lang }, { $set: { isPrimaryLanguageChoice: false } })
        }
        BibleInfos.update(bible._id, { $set: { isPrimaryLanguageChoice: value } })
    },
    'bibleSocieties.remove' (_id) {
        checkLoggedIn()
        check(_id, String)
        BibleSocieties.remove(_id)
    },
    'bibleSocieties.save' (_id, $set) {
        checkLoggedIn()
        check(_id, Match.Maybe(String))
        check($set, Object)
        BibleSocieties.upsert(_id, $set)
    },
    'scrapers.remove' (_id) {
        checkLoggedIn()
        check(_id, String)
        Scrapers.remove(_id)
    },
    'scrapers.save' (_id, $set) {
        checkLoggedIn()
        check(_id, Match.Maybe(String))
        check($set, Object)
        Scrapers.upsert(_id, $set)
    },
    'sendTestEmail' () {
        checkLoggedIn()
        console.log(`sendTestEmail`)
        sendEmail({
            subject: 'Duobiblo Test Email', 
            text: `Test at ${formatDateTime()}`
        })
    },
    'bootstrapBibleInfos' () {
        checkLoggedIn()
        // console.log(`bootstrapBibleInfos`)
        // BibleInfos.remove({})
        // console.log(BibleInfos.find().count())
        if (BibleInfos.find().count()) return
        for(const [code, bible] of Object.entries(bibleInfos)) {
            // console.log(code, bible.code)
            bible._id = code
            bible.bibleSocietyId = getPublisherId(bible)
            delete bible.publisher
            delete bible.publisherLink
            delete bible.copyright            
            if (bible.bibleSocietyId) console.log(bible)
            BibleInfos.insert(bible)
        }
    },
    'bootstrapUsers' () {
        // checkLoggedIn()
        // console.log(`bootstrapUsers`)
        // Meteor.users.remove({})
        // console.log(Meteor.users.find().count())
        // console.log(Meteor.users.find().fetch())
        if (Meteor.users.find().count()) return
        for(const user of Meteor.settings.bootstrap.users) {
            // console.log(user)
            password = user.password
            delete user.password
            Meteor.users.insert(user, (error, id) => {
                if (error) return console.log(error)
                Accounts.setPassword(id, password)
            })
        }
    },
    // values from the old server
    'bootstrapInstalledTranslationsCounter' () {
        checkLoggedIn()
        if (InstalledTranslationsCounter.find().count()) return
        const counts = {
            ASV: 6,
            NTV: 4,
            VULGATE: 3,
            LND: 1,
            GPC2006:1,
            'NVI-PT': 1,
            NVI: 1,
            NIV: 1,
            CCL: 1,
            BDS: 1,
            NLT: 1,
        }
        for( [_id, count] of Object.entries(counts)) {
            console.log(_id, count)
            InstalledTranslationsCounter.upsert({ _id }, { $inc: { count } })
        }
    },
    async 'getAllBibleLanguages'() {
        return await BibleInfos._collection.rawCollection().distinct('lang')
    },
    async 'getUserFromId'() {
        return await Meteor.users.findOne(Meteor.userId())
    },
})

const getPublisherId = (bible) => {
    if (!bible.publisher) return null
    const society = BibleSocieties.findOne({ name: bible.publisher })
    if (society) return society._id
    return BibleSocieties.insert({ name: bible.publisher, url: bible.publisherLink, copyright: bible.copyright })
}

