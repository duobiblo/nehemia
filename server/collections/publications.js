import { Meteor } from 'meteor/meteor'
import { check } from 'meteor/check'

Meteor.publish('contactForms', () => {
    return ContactForm.find({}, { sort: { createdAt: -1 }, limit: 100 })
})

Meteor.publish('contactForm', (_id) => {
    check(_id, String)
    return ContactForm.find({ _id })
})

Meteor.publish('bibleInfos', (selectedLanguage) => {
    const fields = {
        code: 1,
        label: 1,
        languageNameEn: 1,
        languageOrigName: 1,
        lang: 1,
        isPrimaryLanguageChoice: 1,
    }
    return BibleInfos.find({}, { sort: { code: 1 }, fields })
})

Meteor.publish('bibleInfo', (_id) => {
    check(_id, String)
    return BibleInfos.find({ _id })
})
Meteor.publish('bibleSocieties', () => {
    return BibleSocieties.find({}, { sort: { name: 1 } })
})

Meteor.publish('bibleSociety', (_id) => {
    check(_id, String)
    return BibleSocieties.find({ _id })
})

Meteor.publish('scrapers', () => {
    return Scrapers.find({}, { sort: { name: 1 } })
})

Meteor.publish('scraper', (_id) => {
    check(_id, String)
    return Scrapers.find({ _id })
})

Meteor.publish('stats', () => {
    return Stats.find({}, { sort: { createdAt: -1 }, limit: 100 })
})

Meteor.publish('installedTranslationsCounter', () => {
    return InstalledTranslationsCounter.find({}, { sort: { count: -1 } })
})

Meteor.publish('jobResult', (_id) => {
    check(_id, String)
    return JobResults.find({ _id })
})
