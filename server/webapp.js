import { Meteor } from 'meteor/meteor'
import { WebApp } from 'meteor/webapp'
// import { BrowserPolicy } from 'meteor/browser-policy-common'
import bodyParser from 'body-parser'
import { sendEmail } from './email.js'

Meteor.startup(() => {
    // BrowserPolicy.content.allowSameOriginForAll()
    // BrowserPolicy.content.allowConnectSameOrigin()
    // BrowserPolicy.content.allowConnectOrigin(Meteor.absoluteUrl().replace('https://', 'wss://'))
    // BrowserPolicy.content.allowOriginForAll('http://localhost')
    // BrowserPolicy.content.allowOriginForAll('*')
    WebApp.rawConnectHandlers.use((req, res, next) => {
        // res.setHeader('Access-Control-Allow-Origin', '*')
        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3001')
        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3010')
        res.setHeader('Access-Control-Allow-Origin', 'https://www.duobiblo.com')
        res.setHeader('Access-Control-Allow-Origin', 'https://app.duobiblo.com')
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, OPTIONS, HEAD, PUT')
        res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers')
        return next()
    })
    // BrowserPolicy.content.disallowObject()
})

const contactFormHandler = (req, res, next) => {
    const json = JSON.parse(req.body)
    const text = Object.entries(json).reduce((m, [k, v]) => { m += `${k}: ${v}\n`; return m }, '')
    json.createdAt = new Date()
    const { headers } = req
    json.userAgent = headers['user-agent']
    json.acceptLanguage = headers['accept-language']
    json.ip = headers['x-forwarded-for']
    // console.log(req.get('origin'))
    // console.log(req.getHeaders())
    ContactForm.insert(json)
    // if (!headers.origin.includes('localhost')) {
        sendEmail({ subject: 'Duobiblo: Contact Form Submission ', text })
    // }
    res.writeHead(200)
    res.end('Email sent')
}

const statsHandler = (req, res, next) => {
    const json = JSON.parse(req.body)
    json.createdAt = new Date()
    Stats.insert(json)
    if (json.payload.translationsToFetch) {
        for (const translation of json.payload.translationsToFetch) {
            InstalledTranslationsCounter.upsert({ _id: translation }, { $inc: { count: 1 } })
        }
    }
    res.writeHead(200)
    res.end()
}

WebApp.connectHandlers
    .use(bodyParser.urlencoded({ extended: true }))
    .use(bodyParser.text())
    .use(bodyParser.json())
    .use('/api/contactForm', contactFormHandler)
    .use('/api/stats', statsHandler)

// WebApp.connectHandlers
//     .use('/api/contactForm', contactFormHandler)
