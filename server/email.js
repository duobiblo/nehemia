import { SMTPClient } from 'emailjs'

const client = new SMTPClient(Meteor.settings.smtp)

export const sendEmail = params => {
    const message = Object.assign(Meteor.settings.email, params)
    client.send(message, (error, message) => {
        console.log(error || message)
    })
}
